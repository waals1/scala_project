package SBT_scala_package

import org.apache.log4j.Logger
import org.apache.spark.sql.SparkSession



object ScalaDBAmex {
  @transient lazy val logger =Logger.getLogger(getClass.getName)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("Amex Data").master("local[*]")getOrCreate()

    logger.info("Checking s3 bucket for my parquet file")

   // val AwsCredential = spark.read.format("csv").option("header",true)
   //   .option("inferSchema",true).load("dbfs:/FileStore/tables/DatabricksUser.csv")

    //val AccessKey =AwsCredential.select("Access_key_ID").collect.mkString
    //val SecretKey =AwsCredential.select(col = "Secret_access_key").collect.mkString


   // val BucketName = "waalsbucket002"
   // val MountName ="s3data"
   // val EncodedSecretKey = SecretKey.replace("/","%2F")

   // val dbutils = com.databricks.service.DBUtils

    //dbutils.fs.mount(s"s3a://$AccessKey:$EncodedSecretKey@$BucketName",s"/mnt/$MountName")




   val amex_test_df = spark.read.parquet("s3://waals-bucket001/in/test.parquet")
   // amex_test_df.show(10)
   // amex_test_df.write.csv("s3://waals-bucket001/out/csv")

    spark.stop()

  }

}
