package maven_build_scala

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object stackOverflow {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("Log StackOverflow").setLevel(Level.ERROR)
    val spark = SparkSession
      .builder()
      .appName("stack Overflow")
      .master("local[*]")
      .getOrCreate()

    val StackOverflowDF = spark.read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(
        "/Users/waals/IdeaProjects/MavenSparkProject/data/in/2016-stack-overflow-survey-responses.csv"
      )

    val StackOverflowDF_Partitioned = StackOverflowDF.repartition(2)
    val stackOverflowDF_Partitioned_filter = StackOverflowDF_Partitioned.where("age_midpoint < 60")
      .groupBy("collector", "country", "gender", "age_midpoint")
      .count().write.csv("/Users/waals/IdeaProjects/MavenSparkProject/data/out/2016-stack-overflow-survey-responses.csv")


  }

}
