package maven_build_scala

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object scala2 {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("testc scala").setLevel(Level.ERROR)
    val spark = SparkSession.builder
      .appName("airports by latitude")
      .master("local[*]")
      .getOrCreate()

    val airportlatitudeRDD = spark.sparkContext.textFile(
      "/Users/waals/IdeaProjects/MavenSparkProject/data/in/airports.text"
    )

    val airportlatitude2 = airportlatitudeRDD.filter(line =>
      line.split(Utils.COMMA_DELIMITER)(6).toFloat > 40
    )

    val airportlatitude3 = airportlatitude2.map(line => {
      val split = line.split(",")
      split(1) + " , " + split(6)
    })

    val latitudedata = airportlatitude3.saveAsTextFile(
      "/Users/waals/IdeaProjects/MavenSparkProject/data/out/latitude"
    )
  }

}
