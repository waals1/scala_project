package maven_build_scala

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object scala1 {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("testc scala").setLevel(Level.ERROR)
    val spark = SparkSession.builder.appName("demo").master("local[*]").getOrCreate()
    val sampledataRDD1 = spark.sparkContext.textFile("/Users/waals/IdeaProjects/MavenSparkProject/data/in/airports.text")
    val NigeriaAirports = sampledataRDD1.filter(line => line.split(",")(3) == "\"Nigeria\"")
    val NigeriaAirportsRS = NigeriaAirports.map(line => {
      val split = line.split(",")
      split(1) + " ," + split(2) + " ," + split(3)
    })
    val NigeriaAirportData = NigeriaAirportsRS.saveAsTextFile("/Users/waals/IdeaProjects/MavenSparkProject/data/out/nigeria aiports3")


  }

}
